use strict;
use warnings;
use Test::More;
use FreeStyle qw(string array hash);

subtest 'string', sub {
    my $string = string;
    is $string->typeof, 'String';
    is $string->length, 0;
    is $string->val, $$string;
    is $string->val, undef;
    ok $string->is_empty;
    $string->val = 'abc';
    is $string->val, 'abc', "lvalue assignment";
    $string->val('def');
    is $string->val, 'def';
    is $string->length, 3;
    isnt $string->memory_addr, $string->clone->memory_addr;
};

subtest 'array', sub {
    my $array = array;
    is $array->typeof, 'Array';
    is $array->length, -1;
    ok $array->is_empty;
    $array->reset(qw(f o o));
    is $array->length, 2;
    is_deeply [$array->items], [qw(f o o)];
    is_deeply [$array->string_items], [map {string $_} qw(f o o)];
    is $array->items(0), 'f';
    is $array->items(1), 'o';
    is $array->items(2), 'o';
    is_deeply $array->join, string 'foo';
    is_deeply $array->join(','), string 'f,o,o';
    is $array->items(2, 'b'), 'b';
    is_deeply $array->join, string 'fob';
    is_deeply $array->join(','), string 'f,o,b';
    is_deeply $array->string_items(4, 'r'), string('r'), ,'use string items to set value';
    is $array->items(3), undef;
    is_deeply $array->string_items(3), string;
    isnt $array->memory_addr, $array->clone->memory_addr;
};

subtest 'hash', sub {
    my $hash = hash;
    is $hash->typeof, 'Hash';
    ok $hash->is_empty;
    $hash->reset(a => 3, b => 2, c => 1);
    is_deeply [$hash->keys->sort->items], [array(qw( a b c ))->items];
    is_deeply [$hash->keys->sort(sub {$_[1] cmp $_[0]})->string_items], [array(qw( a b c ))->reverse->string_items];
    is $hash->items('a'), 3;
    is_deeply $hash->items(array qw(b c)), array qw(2 1);
    is_deeply $hash->keys->foreach(sub { $hash->items($_->val x2, 1) }, $hash), $hash->clone(aa=>1, bb=>1, cc=>1);
    isnt $hash->memory_addr, $hash->clone->memory_addr;
};

done_testing;
