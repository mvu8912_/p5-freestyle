use strict;
use Test::More;
use FreeStyle qw(string hash);
use URI;

is hash(base_url => URI->new('https://bittrex.com/api/v1.1/public'))
                 ->web
                 ->get('/getmarkets', { foobar => 123 })
                 ->items('debug')
                 ->items('request')
                 ->match(sub { /markets\?(foobar)=123/ })
                 ->items(1)
                 ->val, 'foobar';

ok string('https://bittrex.com/api/v1.1/public')
          ->web
          ->get('/getmarkets')
          ->items('data')
          ->items('result')
          ->count > 0;

done_testing;
