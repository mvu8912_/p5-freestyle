use Test::More;
use_ok 'FreeStyle';
use_ok 'FreeStyle::Plugin';
use_ok 'FreeStyle::Plugin::String';
use_ok 'FreeStyle::Plugin::Array';
use_ok 'FreeStyle::Plugin::Hash';
done_testing;
