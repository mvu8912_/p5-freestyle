package FreeStyle;

use strict;
use base 'Exporter';
use Carp qw(croak);
use Plugin::Loader;
our @plugins  = _load_plugins();
our %callback = _callbacks();
our @EXPORT   = keys %callback;

$SIG{__WARN__} = sub {
  warn @_ if $_[0] !~ /inherited AUTOLOAD/;
};

sub _load_plugins {
    my $loader = Plugin::Loader->new;
    sort map {$loader->load($_); $_}
    sort $loader->find_modules('FreeStyle');
}

sub _callbacks {
    map  { eval { $_->export_methods } } @plugins;
}

sub AUTOLOAD {
    my ($method) = (our $AUTOLOAD =~/::([a-z0-9_]+)$/);
    my $callback = $callback{$method};
    return $callback ? goto &$callback : croak "Undefined subroutine &$AUTOLOAD";
}

1;
