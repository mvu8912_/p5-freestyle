package FreeStyle::Plugin;

use strict;
use base 'FreeStyle';
use Carp qw( croak );
use Capture::Tiny qw( capture );
use Data::Dumper;

sub export_methods {(
    typeof => sub {
        my ($item) = @_;
        my $class = ref $item or return "";
        my ($type) = ($class =~/^FreeStyle::Plugin::(.+)/);
        return $type || $class;
    },
    freestyle => sub {
        my $data = $#_ == -1 ? return string()
                 : $#_ ==  0 ? $_[0]
                 : freestyle(\@_);

        return string($data)  if !defined $data || !typeof($data);
        $data = hash(%$data)  if typeof($data) eq 'HASH';
        $data = array(@$data) if typeof($data) eq 'ARRAY';

        if (typeof($data) eq 'Hash') {
            $data->keys->foreach(sub {
                $data->items($_->val, freestyle($data->items($_->val)));
            });
        }
        elsif (typeof($data) eq 'Array') {
            $data->foreach(sub {
                my ($val, $i) = @_;
                $data->items($i, freestyle($val));
            });
        }
        return $data;
    },
    brew => sub {
        my ($data, $code) = @_;
        local $_ = $data;
        $code ||= sub {$data};
        my $got = freestyle(eval { $code->($data) });
        return hash(
            error => string($@),
            got   => $got,
        );
    },
)};

sub memory_addr { "$_[0]" }

sub clone($) {
    my $self = shift;
    my $class = ref $self;
    $class->new($self->items, @_);
}

sub capture_output($\&) {
    my ( $self, $sub ) = @_;
    my ($output, $error, $errno) = array( capture { $sub->() } )->string_items;
    return hash(
        stdout => $output,
        stderr => $error,
        errno  => $errno,
        stdall => (
            $error->is_empty
                ? $output
                : $output->after("\n----\n")
                         ->after($error),
        ),
    );
}

sub dump {
    my ($self, $setting) = @_;
    my $dumper = Data::Dumper->new([$self]);
    $dumper->Terse(   $setting->{terse}  //= 1);
    $dumper->Indent(  $setting->{indent} //= 1);
    $dumper->Sortkeys($setting->{sort}   ||= sub {[sort keys %{$_[0]}]});
    string($setting->{terse} ? '' : 'my ')->after($dumper->Dump);
}

sub dump_stopper {
    my ($self, $setting) = @_;
    $self->dump($setting)->println;
    exit;
}

1;
