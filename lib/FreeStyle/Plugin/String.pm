package FreeStyle::Plugin::String;

use strict;
use Carp ();
use base 'FreeStyle::Plugin';
use File::Slurp qw( read_file write_file );

require FreeStyle; FreeStyle->import;

sub export_methods {(
    string         => sub { __PACKAGE__->new(@_)      },
    cleanup_string => sub { __PACKAGE__->_cleanup(@_) },
)}

sub new {
    my ($class, $str) = @_;
    return ref($str) ? $str : bless \$str, $class;
}

sub _cleanup {
    my ( $class, $str ) = @_;
    return ( defined($str) && UNIVERSAL::isa( $str, __PACKAGE__ ) )
      ? $$str
      : $str;
}

sub val :lvalue { $#_ == 1 ? ${$_[0]} = $_[1] : ${$_[0]} }

sub before {
    my ($self, $c) = @_;
    $c = $self->_cleanup($c) if ref $c;
    return string("$c$$self");
}

sub after {
    my ($self, $c) = @_;
    $c = $self->_cleanup($c) if ref $c;
    return string($$self . $c);
}

sub split {
    my ($self, $c, $n) = @_;
    if (defined $c && defined $n) {
        return array(map { string($_) } split $c, $$self, $n);
    }
    elsif (defined $c) {
        return array(map { string($_) } split $c, $$self);
    }
    else {
        return array(map { string($_) } split //, $$self);
    }
}

sub length {
    my ($self) = @_;
    $self->defined ? length $$self : 0;
}

sub regexp { goto &match }

sub match($\&) {
    my ($self, $sub) = @_;
    local $_ = $self->clone->val;
    my @matching = map { string($_) } $sub->();
    return array(string($_), @matching);
}

sub replace($\&) {
    my ($self, $sub) = @_;
    local $_ = $self->clone->val;
    my @matching = map { string($_) } $sub->();
    return array($self, string($_), @matching);
}

sub substr {
    my ($self, $offset, $length, $replacement) = @_;
    if (defined $offset && defined $length && defined $replacement) {
        string(substr $$self, $offset, $length, $replacement);
    }
    elsif (defined $offset && defined $length) {
        string(substr $$self, $offset, $length);
    }
    else {
        string(substr $$self, $offset);
    }
}

sub print {
    my ($self) = shift;
    print $$self;
    return $self;
}

sub println {
    my ($self) = shift;
    print $$self, "\n";
    return $self;
}

sub printf {
    my ($self, @vals) = @_;
    $self->sprintf(@vals)->print;
    return $self;
}

sub sprintf {
    my ($self, @vals) = @_;
    return string(sprintf $$self, array(@vals)
                                    ->map(sub { cleanup_array($_) })
                                    ->map(sub { cleanup_string($_) })
                                    ->items);
}

sub is_empty {
    my ($self) = @_;
    $self->defined && $self->length > 0 ? 0 : 1;
}

sub defined($) {
    my ($self) = @_;
    return defined $$self;
}

sub present($$;\&) {
    my ($self, $key, $additional_check) = @_;
    my $basic_check = defined $$self && !$self->is_empty;
    if (!$additional_check) {
        return $basic_check;
    }
    else {
        local $_ = $$self;
        return $basic_check && $additional_check->();
    }
}

sub compare(\&) {
    my ($self, $sub) = @_;
    local $_ = $$self;
    return $sub->() ? 1 : 0;
}

sub clone {
    my ($self) = @_;
    string($$self);
}

sub system {
    my ($self, $verbose) = @_;
    print "> $$self\n" if $verbose;
    $self->capture_output(sub {system $$self});
}

sub exec {
    my ($self, $verbose) = @_;
    print "> $$self\n" if $verbose;
    exec $$self;
}

sub from_file {
    my ($self, $path, @opts) = @_;
    $self->val = read_file $path, @opts;
    return $self;
}

sub to_file {
    my ($self, $path, $mode) = @_;
    if ( $mode ) {
        write_file $path, $mode, $self->val;
    }
    else {
        write_file $path, $self->val;
    }
    return $self;
}

sub uc {
    my ($self) = @_;
    string(uc $self->val);
}

sub lc {
    my ($self) = @_;
    string(lc $self->val);
}

sub ucfirst {
    my ($self) = @_;
    string(ucfirst $self->val);
}

sub lcfirst {
    my ($self) = @_;
    string(lcfirst $self->val);
}

sub is_file {
    my ($self) = @_;
    return -f $$self;
}

sub is_dir {
    my ($self) = @_;
    return -d $$self;
}

sub chdir {
    my ($self) = @_;
    chdir $$self;
    return $self;
}

sub quotemeta {
    my ($self) = @_;
    $$self = quotemeta $$self;
    return $self;
}

sub croak {
    my ($self) = @_;
    Crap::croak($$self);
}

sub die {
    my ($self) = @_;
    die $$self;
}

1;
