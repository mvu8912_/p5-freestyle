package FreeStyle::Plugin::ApiClient;

use strict;
use feature qw(state);
use base 'FreeStyle::Plugin';
use Module::Runtime qw(require_module);
use URI;

my $default_api_module = 'HTTP::API::Client';

sub export_methods {(
    web => sub { __PACKAGE__->new(@_) },
)}

sub new {
    my ($class, $item) = @_;

    my $api_module = $default_api_module;
    my $config    = hash();

    if (defined $item) {
        if (typeof($item) =~/hash/i) {
            $config = $item;
        }
        elsif (!ref $item || typeof($item) eq 'String') {
            $config->items(base_url => URI->new($item->val));
        }
    }

    $class->_load_api_module($api_module);

    return bless { _api => $api_module->new($config->items) }, $class;
}

sub _load_api_module {
    my ($class, $api_module) = @_;

    state $loaded_classes = hash();

    return if !$api_module;
    return if $loaded_classes->exists($api_module);

    if (typeof($api_module) eq 'String') {
        require_module($api_module->val);
    }
    else {
        require_module($api_module);
    }

    $loaded_classes->items($api_module => 'loaded');
}

foreach my $method(qw(get post put head delete)) {
    my $code = sub {
        my $args = array(@_)->flat_list;
        my $self = $args->shift;
        my $path = $args->shift || '';
        my $api  = $self->{_api};
        $api->$method($path, $args->items);
        my $last_response = $api->last_response;
        return freestyle({
            response => $last_response,
            data     => $api->json_response,
            content  => $last_response->decoded_content,
            debug    => {
                request  => $last_response->request->as_string,
                response => $last_response->as_string,
                log      => $last_response->request->as_string . "\n---\n" . $last_response->as_string,
            },
        });
    };
    no strict 'refs';
    *{"FreeStyle::Plugin::ApiClient::$method"} = $code;
    use strict;
}

1;
