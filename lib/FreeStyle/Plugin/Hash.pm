package FreeStyle::Plugin::Hash;

use strict;
use Carp ();
use base 'FreeStyle::Plugin';

require FreeStyle; FreeStyle->import;

sub export_methods {(
    hash => sub {__PACKAGE__->new(@_)}
)}

sub new {
    my $class = shift @_;
    if ($#_ == 0 && ref $_[0]) { return $_[0] }
    bless _constructor_data_normalizer(@_), $class;
}

sub _constructor_data_normalizer {
    my @data = @_;
    my %hash = ();
    for(my $i = 0; $i < $#_; $i += 2) {
        my ($key, $val) = @_[$i..$i+1];
        if (ref $key) {
            if (typeof($key) eq "String") {
                $hash{$key->val} = $val;
            }
            else {
                Carp::croak("Invalid hash key type '$key'");
            }
        }
        else {
            $hash{$key} = $val;
        }
    }
    return \%hash;
}

sub reset {
    my ($self, @new) = @_;
    %$self = @new;
    return $self;
}

sub items($;$$) {
    my ($self, $key, $val) = @_;
    my $mode = $#_ == 0 ? 'all' :
               $#_ == 1 ? 'read' : 'set';
    if ($mode eq "all") {
        return %$self;
    }
    elsif ($mode eq 'read') {
        if (UNIVERSAL::isa($key, 'FreeStyle::Plugin::Array')) {
            return $key->map(sub {$self->{$_}});
        }
        elsif (UNIVERSAL::isa($key, 'FreeStyle::Plugin::String')) {
            return $self->{$$key};
        }
        else {
            return $self->{$key};
        }
    }
    elsif ($mode eq 'set' ) {
        if (UNIVERSAL::isa($key, 'FreeStyle::Plugin::Array')) {
            die 'Cannot set value to multiple keys';
        }
        elsif (UNIVERSAL::isa($key, 'FreeStyle::Plugin::String')) {
            $self->{$$key} = $val;
        }
        else {
            $self->{$key} = $val;
        }
    }
}

sub string_items($;$$) {
    my ($self, $key, $val) = @_;
    my $mode = $#_ == 0 ? 'all' :
               $#_ == 1 ? 'read' : 'set';

    if ($mode eq "all") {
        return map { string($_) } %$self;
    }
    elsif ($mode eq 'read') {
        if (UNIVERSAL::isa($key, 'FreeStyle::Plugin::Array')) {
            return map { string($_) } $key->map(sub {$self->{$_}});
        }
        elsif (UNIVERSAL::isa($key, 'FreeStyle::Plugin::String')) {
            return string($self->{$$key});
        }
        else {
            return string($self->{$key});
        }
    }
    else {
        goto &items;
    }
}

sub exists($$) {
    my ($self, $key) = @_;
    $key = cleanup_string($key) if ref $key;
    return exists $self->{$key};
}

sub present($$;\&) {
    my ($self, $key, $additional_check) = @_;
    $key = cleanup_string($key) if ref $key;
    my $basic_check = exists $self->{$key} && defined $self->{$key};
    if (!$additional_check) {
        return $basic_check ? 1 : 0;
    }
    else {
        return $basic_check && $additional_check->($key, $self->items($key));
    }
}

sub keys($) {
    my ($self) = @_;
    return array(keys %$self);
}

sub values($) {
    my ($self) = @_;
    return array(values %$self);
}

sub each($) {
    my ($self) = @_;
    return array(each %$self);
}

sub is_empty {
    my ($self) = @_;
    return %$self ? 0 : 1;
}

1;
