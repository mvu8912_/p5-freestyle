package FreeStyle::Plugin::FileTemp;

use strict;
use warnings;
use base 'FreeStyle::Plugin';
use File::Temp;

sub export_methods {(
    temp_file => sub { __PACKAGE__->new(@_) },
)}

sub new {
    my ($class, $item, %options) = @_;
    my $file = File::Temp->new(UNLINK => 0, %options);
    my $self = bless { file => $file }, $class;
    return $self->to_file($item);
}

sub binmode {
    my ($self, @mode) = @_;
    binmode $self->{file}, @mode;
    return $self;
}

sub filepath {
    my ($self) = @_;
    return string("$self->{file}");
}

sub filesize {
    my ($self) = @_;
    $self->seek(0,0);
    return -s "$self->{file}";
}

sub getline {
    my ($self) = @_;
    $self->seek(0,0);
    return string($self->{file}->getline);
}

sub seek {
    my ($self, $i, $j) = @_;
    seek($self->{file}, $i, $j);
    return $self;
}

sub from_file {
    my ($self) = @_;
    $self->seek(0,0);
    return array($self->{file}->getlines);
}

sub to_file {
    my ($self, $item, @etc) = @_;
    my $file = $self->{file};
    if (defined $item) {
        if (UNIVERSAL::isa($item, 'FreeStyle::Plugin::String')) {
            $file->print($item->val);
        }
        elsif (UNIVERSAL::isa($item, 'FreeStyle::Plugin::Array')) {
            $file->print($item->items);
        }
        else {
            $file->print($item, @etc);
        }
    }
    return $self;
}

1;
