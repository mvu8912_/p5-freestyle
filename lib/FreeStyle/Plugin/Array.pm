package FreeStyle::Plugin::Array;

use strict;
use Carp ();
use base 'FreeStyle::Plugin';
use File::Slurp qw( read_file write_file );

sub export_methods {(
    array => sub {__PACKAGE__->new(@_)},
    cleanup_array => sub { __PACKAGE__->_cleanup(@_) },
)}

sub new {
    my $class = CORE::shift(@_);
    bless [@_], $class;
}

sub _cleanup {
    my $class = CORE::shift(@_);
    return map { defined && UNIVERSAL::isa($_, __PACKAGE__) ? @$_ : $_  } @_;
}

sub merge {
    my $self = CORE::shift(@_);
    return $self->clone(cleanup_array(@_));
}

sub flat_list {
    my $self = CORE::shift(@_);
    return array(cleanup_array($self->items));
}

sub grep {
    my ( $self, $sub ) = @_;
    return array(
        grep {
            local $_ = cleanup_string($_) if defined $_;
            $sub->($_) } @$self );
}

sub map {
    my ( $self, $sub ) = @_;
    return array(
        map {
            local $_ = cleanup_string($_) if defined $_;
            $sub->($_) } @$self );
}

sub foreach {
    my ($self, $sub, $return) = @_;
    my $loop = $self->offsets;
    foreach my $i($loop->items) {
        local $_ = string($self->items($i));
        my $loop_cmd = $sub->($_, $i, $loop);
        next if !$loop_cmd || $loop_cmd eq 'next';
        last if $loop_cmd eq 'last';
        redo if $loop_cmd eq 'redo';
    }
    return $return // $self;
}

sub shift {
    my ($self) = @_;
    shift @$self;
}

sub pop {
    my ($self) = @_;
    pop @$self;
}

sub unshift {
    my $self = CORE::shift(@_);
    unshift @$self, @_;
    return $self;
}

sub push {
    my $self = CORE::shift(@_);
    push @$self, @_;
    return $self;
}

sub join {
    my ($self, $c) = @_;
    $c //= '';
    return string(join $c, map { cleanup_string($_) } cleanup_array(@$self));
}

sub splice {
    my $self = CORE::shift(@_);
    splice @$self, cleanup_array(@_);
    return $self;
}

sub reset {
    my ($self, @new) = @_;
    @$self = @new;
    return $self;
}

sub items {
    my ($self, $offset, $new_vals) = @_;
    my $mode = $#_ == 0 ? 'all' :
               $#_ == 1 ? 'read' : 'set';
    if ($mode eq 'all') {
        return @$self;
    }
    elsif ($mode eq 'read') {
        if (ref $offset) {
            if (typeof($offset) eq 'Array') {
                return array(@$self[$offset->items]);
            }
            elsif (typeof($offset) eq 'ARRAY') {
                return array(@$self[@$offset]);
            }
            else {
                Carp::croak("Invalid array offset type '$offset'");
            }
        }
        else {
            return $self->[$offset];
        }
    }
    elsif ($mode eq 'set') {
        $self->[$offset] = $new_vals;
    }
}

sub string_items {
    my ( $self, $offset, $new_vals ) = @_;
    my $mode = $#_ == 0 ? 'all' :
               $#_ == 1 ? 'read' : 'set';
    if ($mode eq 'set') {
        $self->items($offset, string($new_vals));
    }
    return ( defined($offset) && $offset ne "" )
      ? string( $self->items($offset) )
      : map { string($_) } $self->items;
}

sub kv_items {
    my ($self) = @_;
    return ( $self->item(0), $self->string_items(1) );
}

sub length {
    my ($self) = @_;
    return $#$self;
}

sub count {
    my ($self) = @_;
    return scalar @$self;
}

sub offsets {
    my ($self) = @_;
    return array(0..$self->length);
}

sub first {
    my ($self) = @_;
    return $self->[0];
}

sub last {
    my ($self) = @_;
    return $self->items($self->length);
}

sub is_empty {
    my ($self) = @_;
    return @$self ? 0 : 1;
}

sub system {
    my ($self, $verbose) = @_;
    my @clean_array = map { cleanup_string($_) } $self->_cleanup(@$self);
    print "> @clean_array\n" if $verbose;
    $self->capture_output(sub {system @clean_array});
}

sub exec {
    my ($self, $verbose) = @_;
    my @clean_array = map { cleanup_string($_) } $self->_cleanup(@$self);
    print "> @clean_array\n" if $verbose;
    exec @clean_array;
}

sub from_file {
    my ($self, $path, @opts) = @_;
    $self->reset(read_file $path, @opts);
}

sub to_file {
    my ($self, $path, $mode) = @_;
    if ( $mode ) {
        write_file $path, $mode, $self->items;
    }
    else {
        write_file $path, $self->items;
    }
    return $self;
}

sub sort {
    my ($self, $sub) = @_;
    return $sub ? array(sort {$sub->($a, $b)} $self->items)
                : array(sort $self->items);
}

sub reverse {
    my ($self) = @_;
    array(reverse $self->items);
}

sub uniq {
    my ($self) = @_;
    my %seen = ();
    array(grep {$_->defined ? !$seen{$_->val}++ : undef} $self->string_items);
}

1;
